<?php

namespace App\Http\Controllers;

use App\Model\SecreteMessage;
use App\User;
use Illuminate\Http\Request;
use phpseclib\Crypt\RSA;


class ActivitiesController extends Controller
{
    private $createKey;
    function __construct()
    {

        $this->createKey = new CreateKeys();
    }
    //get server key
    public function getServerKey(Request $request) {


           if(!$header = $request->header('username')) {
               return response()->json('', 401);
           }


        $check = User::where('username', $header)->first();


        if (!$check){
            return response()->json('you not authorized', 401);
        }

        if (!$this->createKey->getPrivateKey() || !$this->createKey->getPublicKey()){
            if ($this->createKey->createKeys()){
                return $this->createKey->getPublicKey();
            }
        }

        return  $this->createKey->getPublicKey();

    }





    //method to store secrete message
    public function saveSecreteMessage(Request $request) {

        $request->validate([
            'username' => 'required',
            'secretName' => 'required',
            'secretMessage' => 'required'
        ]);

        $username = $request->input('username');

        $publicKey = User::where('username', $username)->first();


        if (!$publicKey) {
            return response()->json('Not allowed', 401);
        }


        $datas = $request->input('encryptedMessage')['encryptedData'];
        $signature = $request->input('encryptedMessage')['signature'];
        $plain = $request->input('secretMessage');
        $messageName = $request->input('secretName');


        $ok = $this->validateSignature($publicKey, $datas, $signature);

        $rsa = new RSA();
        $rsa->loadKey($this->createKey->getPrivateKey());


        if ($ok == 'good') {

            if ($rsa->decrypt(base64_decode($datas))){

                    $message = new SecreteMessage([
                        'username' => $username,
                        'secretName' => $messageName,
                        'encryptedSecret' => $datas

                    ]);

                    if ($message->save()){
                        return response()->json('Secret Saved', 201);
                    }

            }else{
                return response()->json('Its not verified', 400);
            }
        } else {
            return response()->json(openssl_error_string(), 400);
        }


    }

    public function getSecret(Request $request) {



        $username = $request->header('usernameRaw');
        $signedUsername = $request->header('signedUsername');
        $secretName = $request->header('secretName');
        $signedSecretName = $request->header('signedSecretName');




        $publicKey = User::where('username', $username)->first();

        if (!$publicKey){
            return response()->json('', 404);
        }

        $okUsername = $this->validateSignature($publicKey, $username, $signedUsername) ? 'good' : 'bad';
        $ok = $this->validateSignature($publicKey, $secretName, $signedSecretName) ? 'good' : 'bad';


        if ($okUsername == 'good' && $ok == 'good'){

            $message = SecreteMessage::where('secretName', $secretName)->first();
            if ($message){

                $decryptedMessage = $this->decrypt($message->encryptedSecret);
                $encryptedMessage = $this->encrypt($publicKey->publicKey, $decryptedMessage);
                //return $publicKey;
                return response()->json($encryptedMessage, 200);
            } else{
                return response()->json('its bad', 404);
            }

        } else {
            return response()->json('bad', 400);
        }

    }

    /**
     * @param $publicKey
     * @param $datas
     * @param $signature
     * @return array
     */
    public function validateSignature($publicKey, $datas, $signature): string
    {
        $rsa = new RSA();
        $rsa->loadKey($publicKey->publicKey); // public key;
        $rsa->setSignatureMode(RSA::SIGNATURE_PKCS1);
        $rsa->setHash('sha256');

        // return base64_decode($signature);
        $ok = $rsa->verify($datas, base64_decode($signature)) ? 'good' : 'bad';
        return $ok;
    }

    private function decrypt($data) {

        $rsa = new RSA();
        $privatekey = $this->createKey->getPrivateKey();
        $rsa->loadKey($privatekey);


        return $rsa->decrypt(base64_decode($data));
    }

    public function encrypt($publicKey, $data) {

        $rsa = new RSA();
       // $rsa->setEncryptionMode(RSA::ENCRYPTION_PKCS1);
        $rsa->loadKey($publicKey);
        $encrypt = $rsa->encrypt($data);
        return base64_encode($encrypt);

    }
}
