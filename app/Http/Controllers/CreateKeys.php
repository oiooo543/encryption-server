<?php


namespace App\Http\Controllers;


use League\Flysystem\FileNotFoundException;
use phpseclib\Crypt\RSA;

class CreateKeys
{
    public function getPublicKey(){
        $file = $this->getPublicKeyFile();
        if (!file_exists($file)) {
            throw new FileNotFoundException(_('file missing'));
        }

        return file_get_contents($file);
    }

    public function getPrivateKey() {
        $file = $this->getPrivateKeyFile();
        if (!file_exists($file)) {
            throw new FileNotFoundException(_('file missing'));
        }
        return file_get_contents($file);
    }

    public function  createKeys() {
        $rsa = new RSA();
        $keys = $rsa->createKey(1024);
        $private = file_put_contents($this->getPrivateKeyFile(), $keys['privatekey']);
        $pub = file_put_contents($this->getPublicKeyFile(), $keys['publickey']);
        return $private && $pub;
    }

    private function getPublicKeyFile(){
        return base_path('publickey.pem');
    }

    private function  getPrivateKeyFile() {
        return base_path('privatekey.pem');
    }

}
