<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SecreteMessage extends Model
{
    protected $fillable = ['username', 'secretName', 'encryptedSecret'];
}
