<?php

namespace Tests\Unit;

use Tests\TestCase;

class getServerKeyTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testGetKeyWithoutHeader()
    {
        $response = $this->json('GET', 'api/v1/getServerKey');

        $response
            ->assertStatus(401);
    }

    public function testGetKeyWrongHeader()
    {
        $response = $this->withHeaders([
            'publicKey' => '123456',
        ])->json('GET', 'api/v1/getServerKey');

        $response
            ->assertStatus(401)
            ->assertSeeText('you not authorized');
    }

}
